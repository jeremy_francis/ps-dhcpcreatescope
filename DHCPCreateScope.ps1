# - Functions to call based on scope type and location
function USVoiceOption {
	Set-DHCPServerv4OptionValue -ComputerName $dhcpserver -ScopeId $scopeID -OptionID 150 -Value 10.8.140.10,10.8.140.11
	}
function EUVoiceOption {
	Set-DHCPServerv4OptionValue -OptionID 150 -Value 10.1.140.10,10.1.140.11
	}
function APVoiceOption {
	Set-DHCPServerv4OptionValue -ComputerName $dhcpserver -ScopeId $scopeID -OptionID 150 -Value 10.73.140.10,10.73.140.11
	}
function USAPMGMTOption {
	Set-DHCPServerv4OptionValue -ComputerName $dhcpserver -ScopeId $scopeID -OptionID 043 -Value 0x00,0xF1,0x04,0x0A,0x08,0x02,0x0A
	}
function EUAPMGMTOption {
	Set-DHCPServerv4OptionValue -ComputerName $dhcpserver -ScopeId $scopeID -OptionID 043 -Value 0x00,0xF1,0x04,0x0A,0x89,0x02,0x0A
	}
function APAPMGMTOption {
	Set-DHCPServerv4OptionValue -ComputerName $dhcpserver -ScopeId $scopeID -OptionID 043 -Value 0x00,0xF1,0x04,0x0A,0x49,0x02,0x0A
	}
function WirelessLease {
	Set-DHCPServerv4Scope -ComputerName $dhcpserver -ScopeId $scopeID -LeaseDuration 2.00:00:00
	}

# - Create OK and Cancel buttons for use in forms
Add-Type -AssemblyName System.Windows.Forms
Add-Type -AssemblyName System.Drawing
	
$OKButton = New-Object System.Windows.Forms.Button
$OKButton.Location = New-Object System.Drawing.Point(75,120)
$OKButton.Size = New-Object System.Drawing.Size(75,23)
$OKButton.Text = "OK"
$OKButton.DialogResult = [System.Windows.Forms.DialogResult]::OK	

$CancelButton = New-Object System.Windows.Forms.Button
$CancelButton.Location = New-Object System.Drawing.Point(150,120)
$CancelButton.Size = New-Object System.Drawing.Size(75,23)
$CancelButton.Text = "Cancel"
$CancelButton.DialogResult = [System.Windows.Forms.DialogResult]::Cancel	
	
# - Ask user to select DHCP Server from list
$form = New-Object System.Windows.Forms.Form 
$form.Text = "Select a DHCP Server"
$form.Size = New-Object System.Drawing.Size(300,200) 
$form.StartPosition = "CenterScreen"
$form.AcceptButton = $OKButton
$form.Controls.Add($OKButton)
$form.CancelButton = $CancelButton
$form.Controls.Add($CancelButton)

$label = New-Object System.Windows.Forms.Label
$label.Location = New-Object System.Drawing.Point(10,20) 
$label.Size = New-Object System.Drawing.Size(280,20) 
$label.Text = "Please select a DHCP server:"
$form.Controls.Add($label) 

$listBox = New-Object System.Windows.Forms.ListBox 
$listBox.Location = New-Object System.Drawing.Point(10,40) 
$listBox.Size = New-Object System.Drawing.Size(260,20) 
$listBox.Height = 80

[void] $listBox.Items.Add("USDC1AD01")
[void] $listBox.Items.Add("EUDC1AD01")
[void] $listBox.Items.Add("APDC1AD01")

$form.Controls.Add($listBox) 
$form.Topmost = $True
$result = $form.ShowDialog()

if ($result -eq [System.Windows.Forms.DialogResult]::OK){
    $dhcpserver = $listBox.SelectedItem
    }

# - Ask user to select Scope Type from list
$form = New-Object System.Windows.Forms.Form 
$form.Text = "Select Scope Type"
$form.Size = New-Object System.Drawing.Size(300,200) 
$form.StartPosition = "CenterScreen"
$form.AcceptButton = $OKButton
$form.Controls.Add($OKButton)
$form.CancelButton = $CancelButton
$form.Controls.Add($CancelButton)

$label = New-Object System.Windows.Forms.Label
$label.Location = New-Object System.Drawing.Point(10,20) 
$label.Size = New-Object System.Drawing.Size(280,20) 
$label.Text = "What type of scope is this?"
$form.Controls.Add($label) 

$listBox = New-Object System.Windows.Forms.ListBox 
$listBox.Location = New-Object System.Drawing.Point(10,40) 
$listBox.Size = New-Object System.Drawing.Size(260,20) 
$listBox.Height = 80

[void] $listBox.Items.Add("Data")
[void] $listBox.Items.Add("Voice")
[void] $listBox.Items.Add("APMGMT")
[void] $listBox.Items.Add("RFS")

$form.Controls.Add($listBox) 
$form.Topmost = $True
$result = $form.ShowDialog()

if ($result -eq [System.Windows.Forms.DialogResult]::OK){
    $ScopeType = $listBox.SelectedItem
    }	

# - Ask user to select Connection Type from list
$form = New-Object System.Windows.Forms.Form 
$form.Text = "Select Connection Type"
$form.Size = New-Object System.Drawing.Size(300,200) 
$form.StartPosition = "CenterScreen"
$form.AcceptButton = $OKButton
$form.Controls.Add($OKButton)
$form.CancelButton = $CancelButton
$form.Controls.Add($CancelButton)

$label = New-Object System.Windows.Forms.Label
$label.Location = New-Object System.Drawing.Point(10,20) 
$label.Size = New-Object System.Drawing.Size(280,20) 
$label.Text = "Will this scope serve Wired or Wireless clients?"
$form.Controls.Add($label) 

$listBox = New-Object System.Windows.Forms.ListBox 
$listBox.Location = New-Object System.Drawing.Point(10,40) 
$listBox.Size = New-Object System.Drawing.Size(260,20) 
$listBox.Height = 80

[void] $listBox.Items.Add("Wired")
[void] $listBox.Items.Add("Wireless")

$form.Controls.Add($listBox) 
$form.Topmost = $True
$result = $form.ShowDialog()

if ($result -eq [System.Windows.Forms.DialogResult]::OK){
    $ConnectionType = $listBox.SelectedItem
    }	

# - Ask user to enter Site ID, Scope ID and Subnetmask	
[void][System.Reflection.Assembly]::LoadWithPartialName('Microsoft.VisualBasic')
$siteID = [Microsoft.VisualBasic.Interaction]::InputBox("Enter 7 digit site ID`n`nExamples:`n`nEUNLECO`nAPJPKOB`nAMUSWTL", "Site ID", "")
$scopeID = [Microsoft.VisualBasic.Interaction]::InputBox("Enter Scope ID like 10.1.1.0", "Scope ID", "10.")
$subnetmask = [Microsoft.VisualBasic.Interaction]::InputBox("Enter Subnetmask", "Subnetmask", "255.255.255.0")

# - Create Scope Name
$scopename = $siteID + "-" + $ConnectionType + "-" + $ScopeType

# - Convert IP entered as ScopeID to array and then create Router Address
$IPByte = $ScopeID.Split(".")
$router = ($IPByte[0]+"."+$IPByte[1]+"."+$IPByte[2]+"."+"1")

# - Create Start and End addresses based on Scope Type
if ($ScopeType -eq "Data"){
	$startrange = ($IPByte[0]+"."+$IPByte[1]+"."+$IPByte[2]+"."+"30")
	$endrange = ($IPByte[0]+"."+$IPByte[1]+"."+$IPByte[2]+"."+"254")
	}
if ($ScopeType -eq "Voice"){
	$startrange = ($IPByte[0]+"."+$IPByte[1]+"."+$IPByte[2]+"."+"30")
	$endrange = ($IPByte[0]+"."+$IPByte[1]+"."+$IPByte[2]+"."+"254")
	}
if ($ScopeType -eq "APMGMT"){
	$startrange = ($IPByte[0]+"."+$IPByte[1]+"."+$IPByte[2]+"."+"200")
	$endrange = ($IPByte[0]+"."+$IPByte[1]+"."+$IPByte[2]+"."+"254")
	}	
if ($ScopeType -eq "RFS"){
	$startrange = ($IPByte[0]+"."+$IPByte[1]+"."+$IPByte[2]+"."+"30")
	$endrange = ($IPByte[0]+"."+$IPByte[1]+"."+$IPByte[2]+"."+"254")
	}
	
# - Print scope settings on screen for user to verify
Write-Host
Write-Host ----------Preconfigured Settings----------- -foregroundcolor "yellow"
Write-Host
Write-Host Server: {}{}{}{}{}{}{}{} $dhcpserver -foregroundcolor "yellow"
Write-Host Site ID: {}{}{}{}{}{}{} $SiteID -foregroundcolor "yellow"
Write-Host Type: {}{}{}{}{}{}{}{}{}{} $ScopeType -foregroundcolor "yellow"
Write-Host Connection: {}{}{}{} $ConnectionType -foregroundcolor "yellow"
Write-Host Scope Name: {}{}{}{} $scopename -foregroundcolor "yellow"
Write-Host Scope ID: {}{}{}{}{}{} $scopeID -foregroundcolor "yellow"
Write-Host IP Range: {}{}{}{}{}{} $startrange - $endrange -foregroundcolor "yellow"
Write-Host Subnetmask: {}{}{}{} $subnetmask -foregroundcolor "yellow"
Write-Host Router: {}{}{}{}{}{}{}{} $router -foregroundcolor "yellow"
Write-Host
Write-Host ---------/Preconfigured Settings----------- -foregroundcolor "yellow"
Write-Host
Write-Host
Write-Host
Write-Host Type in y to continue or any key to cancel...
Write-Host

# - Prompt the user to enter "y" to confirm settings and create scope
$input = [Microsoft.VisualBasic.Interaction]::InputBox("Type in y to continue `n or any key to cancel...", "Create Scope", "")

# - If settings confirmed, create scope and configure options
if(($input) -eq "y" ){	
    $ErrorActionPreference = "Stop"
	Add-DHCPServerv4Scope -ComputerName $dhcpserver -EndRange $endrange -Name $scopename -StartRange $startrange -SubnetMask $subnetmask -State Active
	Set-DHCPServerv4OptionValue -ComputerName $dhcpserver -ScopeId $scopeID -Router $router
	Set-DhcpServerv4DnsSetting -ComputerName $dhcpserver -ScopeId $scopeID -UpdateDnsRRForOlderClients $True
		if ($ConnectionType -eq "Wireless"){
			WirelessLease
			}
		if (($ScopeType -eq "Voice") -and ($dhcpserver -eq "USDC1AD01")){
			USVoiceOption
			}
		if (($ScopeType -eq "Voice") -and ($dhcpserver -eq "EUDC1AD01")){
			EUVoiceOption
			}
		if (($ScopeType -eq "Voice") -and ($dhcpserver -eq "APDC1AD01")){
			APVoiceOption
			}
		if (($ScopeType -eq "APMGMT") -and ($dhcpserver -eq "USDC1AD01")){
			USAPMGMTOption
			}
		if (($ScopeType -eq "APMGMT") -and ($dhcpserver -eq "EUDC1AD01")){
			EUAPMGMTOption
			}		
		if (($ScopeType -eq "APMGMT") -and ($dhcpserver -eq "APDC1AD01")){
			APAPMGMTOption
			}
        if ($dhcpserver -eq "APDC1AD01"){
			Add-DhcpServerv4FailoverScope -ComputerName $dhcpserver -Name "APDC1 - USDC2" -ScopeId $scopeID
			}
        if ($dhcpserver -eq "EUDC1AD01"){
			Add-DhcpServerv4FailoverScope -ComputerName $dhcpserver -Name "EUDC1 - USDC2" -ScopeId $scopeID
			}
        if ($dhcpserver -eq "USDC1AD01"){
			Add-DhcpServerv4FailoverScope -ComputerName $dhcpserver -Name "USDC1 - USDC2" -ScopeId $scopeID
			}
								
    Write-Host
    Write-Host
    Write-Host Created Scope $scopename  on Server $dhcpserver -foregroundcolor "green"
    Write-Host
    Write-Host ---------------Settings-------------------- -foregroundcolor "green"
    Write-Host ------------------------------------------- -foregroundcolor "green"
    Write-Host
    Write-Host Server: {}{}{}{}{}{}{}{} $dhcpserver -foregroundcolor "green"
	Write-Host Site ID: {}{}{}{}{}{}{} $SiteID -foregroundcolor "green"
	Write-Host Type: {}{}{}{}{}{}{}{}{}{} $ScopeType -foregroundcolor "green"
	Write-Host Connection: {}{}{}{} $ConnectionType -foregroundcolor "green"
	Write-Host Scope Name: {}{}{}{} $scopename -foregroundcolor "green"
	Write-Host Scope ID: {}{}{}{}{}{} $scopeID -foregroundcolor "green"
	Write-Host IP Range: {}{}{}{}{}{} $startrange - $endrange -foregroundcolor "green"
	Write-Host Subnetmask: {}{}{}{} $subnetmask -foregroundcolor "green"
	Write-Host Router: {}{}{}{}{}{}{}{} $router -foregroundcolor "green"
    Write-Host
    Write-Host ------------------------------------------- -foregroundcolor "green"
    Write-Host --------------/Settings-------------------- -foregroundcolor "green"
	}

else{
	exit
	}